//
//  TivoApi.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 31/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation

import Alamofire

let BASE_URL = "http://api.rovicorp.com/TVlistings/v9/listings/"


class TivoParams {
	
	var format = "json"
	var local = "fr-BE"
	var apiKey = "vswvafgqz76mnhvcep4gzf4k"
	
}

class LinearParams : TivoParams {
	
	var serviceId: Int!
	var sourceId = ""
	var inProgress = "true"
	var duration = 240
	var startDate :String!
	
	required  init(serviceId:Int!,sourceIds:[Int]!) {
		super.init()
		self.serviceId = serviceId
		for id in sourceIds {
			sourceId = sourceId + "\(id),"
		}
		
		let t = "yyyy-MM-dd'T'HH:mm:ss'Z'"
		let formatter = DateFormatter()
		formatter.dateFormat = t//"yyyy-MM-dd'T'HH:mm:ss.SSSZ"
		
		
		startDate = formatter.string(from: Date())
	}
	
}

class TivoApi {
	
	
	
	static func services(postalCode : String = "0",local : String = "fr-BE",coutryCode:String = "BE",format: String = "json", apiKey : String = "vswvafgqz76mnhvcep4gzf4k",completion: @escaping (_ result: Any?,_ error : Error?) -> Void){
		let params = ["local" : local , "countrycode":coutryCode , "format": format, "apikey": apiKey]
		
		TivoApi.request(serviceName: "services", mainParams: "postalcode/\(postalCode)", parameters: params,completion: completion)
		
	}
	
	static func linearSchedule(parameter : LinearParams,completion: @escaping (_ result: Any?,_ error : Error?) -> Void){
		
		let params: Parameters = ["local" : parameter.local ,  "format": parameter.format, "apikey": parameter.apiKey , "sourceid":parameter.sourceId, "duration":"\(parameter.duration)",  "inprogress":parameter.inProgress]
		
		TivoApi.request(serviceName: "listings", mainParams: "linearschedule/\(parameter.serviceId!)", parameters: params,completion: completion)
		
	}
	
	static func request(serviceName:String,mainParams:String,parameters : Parameters,completion: @escaping (_ result: Any?,_ error : Error?) -> Void){
		
		let parameters: Parameters = parameters
		let url = BASE_URL + "\(mainParams)/info"
		let headers: HTTPHeaders = [
			"Accept":"application/json",
			"Content-Type": "application/json"
		]
		
		Alamofire.request(url, parameters: parameters, headers: headers).validate().responseJSON { response in
			switch response.result {
			case .success:
				print("Validation Successful")
				if let json = response.result.value {
					
					completion(json,nil)
    }
			case .failure(let error):
				completion(nil,error)
				print(error)
			}
		}
	}
	
}
