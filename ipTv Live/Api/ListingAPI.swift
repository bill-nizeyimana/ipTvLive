//
//  ListingAPI.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 31/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import Alamofire

class ListingAPI: NSObject {
	
	
	static func getListing(sourceId: Int){
		let parameters: Parameters = ["sourceId": sourceId]
		let url = "http://api.backendless.com/v1/services/TVListing/1.0.0/Listing"
		let headers: HTTPHeaders = [
			"application-id": "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==",
			"secret-key": "B3FF5803-C634-C737-FF19-D541C7823800",
			"Accept":"application/json",
			"Content-Type": "application/json"
		]
		
		Alamofire.request(url, parameters: parameters, headers: headers).validate().responseJSON { response in
			switch response.result {
			case .success:
				print("Validation Successful")
				if let json = response.result.value {
					print("JSON: \(json)")
				}
			case .failure(let error):
				print(error)
			}
		}
	}

}
