//
//  TvGuide.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 31/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation
import RealmSwift
class TvGuide  {
	
	static func fetchAllGuides(mainPlaylist : MainPlaylist!) {
		let realm1 = try! Realm()
		let channels = mainPlaylist.masterPlaylist?.channels.filter("channelId != '-1' AND channelId != nil")
		
		var listId = [Int]()
		for channel in channels! {
			listId.append(Int(channel.channelId!)!)
			
			try! realm1.write {
				channel.programs = List<Program>()
				realm1.add(channel, update: true)
			}
		}
		
		let progs = realm1.objects(Program.self)
		
		try! realm1.write {
			realm1.delete(progs)
		}

		
		let parm = LinearParams(serviceId: 892151, sourceIds: listId)
		
		DispatchQueue.global(qos: .background).async {
			
		
		TivoApi.linearSchedule(parameter: parm) { (data, error) in
			
			
			if let d = data {
				
				let JSON = d as! NSDictionary
				let programs = JSON["LinearScheduleResult"] as! NSDictionary
				let v = programs["Schedule"] as! NSDictionary
				let Airings = v["Airings"] as! [NSDictionary]
				let TimeZones = v["TimeZones"] as! [NSDictionary]
				
				let realm = try! Realm()
				for airing in Airings {
					
					let prog = Program()
					prog.programId = (airing["ProgramId"] as? String)!
					if let copy = airing["Copy"] as? String {
						prog.desc = copy
					}
					if let ep = airing["EpisodeTitle"] as? String {
						prog.episodeTitle = ep
					}
					if let title = airing["Title"] as? String{
						prog.title = title

					}
					
					if let off = TimeZones[0]["Offset"] as? Int{
						prog.offset = off
					}
					
					prog.duration = airing["Duration"] as! Int
					prog.sourceId = airing["SourceId"] as! Int
					prog.serviceId = airing["ServiceId"] as! Int
					
					if let d =  airing["AiringTime"] as? String {
						
						let t = "yyyy-MM-dd'T'HH:mm:ss'Z'"
						let formatter = DateFormatter()
						formatter.dateFormat = t
						if let date = formatter.date(from: d) {
							let calendar = Calendar.current
							prog.startDate = calendar.date(byAdding: .minute, value: prog.offset, to: date)
							
							let endDate = calendar.date(byAdding: .minute, value: prog.duration+prog.offset, to: date)
							prog.endDate = endDate!
						}
					}
					
					let predicate = NSPredicate(format: "channelId == %@ ", "\(prog.sourceId)")
					let channels = realm.objects(Channel.self).filter(predicate)
					
					for channel in channels {
						
						try! realm.write {
							channel.programs.append(prog)
							
						}
					}
				}
				
				
				
				
				
				
			}else {
				print(error ?? "noerro")
			}
		}
		
		
	}
	}
	
}
