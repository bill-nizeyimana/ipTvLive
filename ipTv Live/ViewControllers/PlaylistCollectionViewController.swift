//
//  PlaylistCollectionViewController.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 18/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import RealmSwift

private let reuseIdentifier = "Cell"

class PlaylistCollectionViewController: UICollectionViewController {
	
	var mainPlaylist : Results<MainPlaylist>?
	var selPlaylist : MainPlaylist?
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.collectionView?.register(UINib(nibName: "MainPlaylistCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
		
		let realm = try! Realm()
		
		let mp = realm.objects(MainPlaylist.self)
		mainPlaylist = mp
		
		if (mainPlaylist?.count)! > 0 {
			
				IPTvUtils.updateVOD(mainPlaylist: self.mainPlaylist?[0])
			
			
			//IPTvUtils.updateTV(mainPlaylist: mainPlaylist?[0])
			
		}
		
		
		let title = "-1 tvg-ID=\"\" tvg-name=\"[TN]First-TV\" tvg-logo=\"\" group-title=\"Chaines arabes\",[TN]First-TV"

		if let rGroup = title.range(of: "group-title=\""){
			print(rGroup)
			let rm = title.substring(from: rGroup.upperBound)
			print(rm)
			if let rmGroup = rm.range(of: "\""){
				print(rmGroup)
				let group = rm.substring(to: rmGroup.lowerBound)
				print(group)
			}
		}

	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		let realm = try! Realm()
		
		let mp = realm.objects(MainPlaylist.self)
		mainPlaylist = mp
		
		collectionView?.reloadData()

	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using [segue destinationViewController].
	// Pass the selected object to the new view controller.
	}
	*/
	
	// MARK: UICollectionViewDataSource
	
	override func numberOfSections(in collectionView: UICollectionView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return 2
	}
	
	
	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		// #warning Incomplete implementation, return the number of items
		switch section {
		case 0:
			return (mainPlaylist?.count)!
		case 1:
			return 1
		default:
			return 0
		}
	}
	
	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MainPlaylistCollectionViewCell
		
		// Configure the cell
		
		
		
		switch indexPath.section {
		case 0:
			let mPl = mainPlaylist?[indexPath.row]
			cell.title = (mPl?.name)!
			break
		case 1:
			cell.title = "+"
			break
		default: break
			
		}
		return cell
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if(segue.identifier == "showLive"){
			let dest = segue.destination as! MainTabBarViewController
			dest.mainPlaylist = self.selPlaylist
			let com1 = dest.viewControllers![0] as! LiveViewController
			com1.mainPlaylist = self.selPlaylist
			let com2 = dest.viewControllers![1] as! VODViewController
			com2.mainPlaylist = self.selPlaylist
			let com3 = dest.viewControllers![2] as! ChannelSettingViewController
			com3.mainPlaylist = self.selPlaylist
			
		}
	}
	
	func selectPlaylist(playlist : MainPlaylist)  {
		selPlaylist = playlist
		performSegue(withIdentifier: "showLive", sender: self)
	}
	
	// MARK: UICollectionViewDelegate
	
	/*
	// Uncomment this method to specify if the specified item should be highlighted during tracking
	override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
	return true
	}
	*/
	
	/*
	// Uncomment this method to specify if the specified item should be selected
	override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
	return true
	}
	*/
	
	/*
	// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
	override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
	return false
	}
	
	override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
	return false
	}
	
	override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
	
	}
	*/
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		switch indexPath.section {
		case 0:
			let pl = mainPlaylist?[indexPath.row]
			selectPlaylist(playlist: pl!)
			break
		case 1:
			self.performSegue(withIdentifier: "addPlayList", sender: self)
			break
		default:
			break
		}
	}
	
}
