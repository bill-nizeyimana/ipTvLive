//
//  AddPlaylistViewController.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 19/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import RealmSwift

class AddPlaylistViewController: UIViewController {
	
	@IBOutlet weak var txtTitle: UITextField!
	@IBOutlet weak var txtUrl: UITextField!
	@IBOutlet weak var btnCancel: UIButton!
	@IBOutlet weak var btnSave: UIButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		btnCancel.addTarget(self, action: #selector(AddPlaylistViewController.cancel(_:)), for: .primaryActionTriggered)
		btnSave.addTarget(self, action: #selector(AddPlaylistViewController.save(_:)), for: .primaryActionTriggered)
		// Do any additional setup after loading the view.
		txtTitle.text = "horizon"
		txtUrl.text = "http://s1.live-tv-direct.com:8000/get.php?username=k8aFLWjgUg&password=OIwffWSIfu&type=m3u_plus&output=m3u8"
	}
	
	func cancel(_ sender: Any){
		dismiss(animated: true, completion: nil)
	}
	
	
	func fixName(name: String) -> String {
		var nameFix = name
		nameFix = nameFix.replacingOccurrences(of: "_", with: " ")
		if let rGroup = nameFix.range(of: ":"){
			nameFix = nameFix.substring(from: rGroup.upperBound)
		}
		
		if let rGroup = nameFix.range(of: "\r"){
			nameFix = nameFix.substring(to: rGroup.lowerBound)
		}
		
		nameFix = nameFix.replacingOccurrences(of: " fr", with: "")
		nameFix = nameFix.replacingOccurrences(of: " FR", with: "")
		
		nameFix = nameFix.trimmingCharacters(in: .whitespaces)
		
		return nameFix
	}
	
	func getTivoChannel(channel : Channel) -> TivoChannel? {
		var tivo : TivoChannel? = nil
		
		let realm1 = try! Realm()
		
		var flat = channel.flatName;
		flat = flat.replacingOccurrences(of: "Full HD", with: "")
		flat = flat.replacingOccurrences(of: " DECALE-9", with: "")
		flat = flat.replacingOccurrences(of: " DECALE-6", with: "")
		flat = flat.replacingOccurrences(of: " -6", with: "")
		flat = flat.replacingOccurrences(of: " fr", with: "")
		let predicate = NSPredicate(format: "DisplayName CONTAINS[c] %@ OR flatName CONTAINS[c] %@ ", channel.flatName, flat)
		
		let tanDogs = realm1.objects(TivoChannel.self).filter(predicate)
		
		if tanDogs.count > 0 {
			tivo = tanDogs[0]
		}
		return tivo
	}
	
	func save(_ sender: Any){
		let realm = try! Realm()
		
		let playlist = MainPlaylist()
		let masterPlaylist = MasterPlaylist()
		
		
		playlist.name = txtTitle.text!
		playlist.url = txtUrl.text!
		playlist.masterPlaylist = masterPlaylist
		
		masterPlaylist.name = txtTitle.text!
		masterPlaylist.url = txtUrl.text!
		
		
		let url = txtUrl.text
		let  model = try? M3U8PlaylistModel(url: url)
		
		/*//let  model = try? M3U8PlaylistModel(url: url)
		let path = Bundle.main.path(forResource: "channels", ofType: "m3u")
		let url1 = URL(fileURLWithPath: path!)
		let srt = try?  String(contentsOf: url1, encoding: .utf8)
		let  model = try? M3U8PlaylistModel(string: srt, baseURL: nil)*/
		var groups = [String:List<Channel>]()
		
		if(model != nil){
			masterPlaylist.originalText = (model?.mainMediaPl.originalText)!
			let segList = model?.mainMediaPl.segmentList
			var count = 1
			count = Int((segList?.count)!)
			var SGroupName = ""
			
			for index in 0...count-1 {
				let info = segList?.segmentInfo(at: UInt(index))
				let title = (info?.title)!
				
				let sub = "\(title[title.startIndex])"
				
				if(sub != "#"){
					
					let channel = Channel()
					channel.name = title
					channel.url = (info?.uri)!
					channel.duration = (info?.duration)!
					channel.tagInfo = (info?.tagInfo)!
					channel.name = fixName(name: channel.name)
					
					let str: NSMutableString = NSMutableString(string: channel.name)
					CFStringTransform(str, nil, kCFStringTransformStripDiacritics, false)
					channel.flatName = str as String
					
					if let rGroup = channel.tagInfo.range(of: "tvg-logo=\""){
						let rm = channel.tagInfo.substring(from: rGroup.upperBound)
						if let rmGroup = rm.range(of: "\""){
							let tvgLogo = rm.substring(to: rmGroup.lowerBound)
							channel.tvgLogo = tvgLogo
						}
					}
					
					if let tivo = getTivoChannel(channel: channel) {
						channel.tivoChannel = tivo
						channel.tvgLogo = tivo.ImageUrl
						channel.sourceId = Int(tivo.id!)!
						channel.channelId = tivo.id
					}
					
					if (channel.url.lowercased().range(of:"ts") == nil) && (channel.url.lowercased().range(of:"m3u8") == nil) {
						channel.isVideo = true
					}
					if let rGroup = channel.tagInfo.range(of: "group-title=\""){
						
						let rm = channel.tagInfo.substring(from: rGroup.upperBound)
						if let rmGroup = rm.range(of: "\""){
							
							var groupName = rm.substring(to: rmGroup.lowerBound)
							groupName = Utils.removeSpecialCharsFromString(text: groupName)
							groupName = groupName.trimmingCharacters(in: .whitespaces)
							
							
							if(groupName != ""){
								let f = "\(groupName[groupName.startIndex])"
								if(f != "#"){
									
									if let plList = groups[groupName]{
										plList.append(channel)
									}else {
										groups[groupName] = List<Channel>()
										groups[groupName]?.append(channel)
									}
								}
							}
							
						}
					}
					
			  var isChan = true
					
					var sub = "\(channel.name[channel.name.startIndex])"
					
					sub = sub.trimmingCharacters(in: .whitespaces)
					let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
					if sub.rangeOfCharacter(from: characterset.inverted) != nil {
						SGroupName = Utils.removeSpecialCharsFromString(text: channel.name)
						SGroupName = SGroupName.trimmingCharacters(in: .whitespaces)
						isChan = false
					}
					
					/*if(SGroupName != ""){
					
					if let plList = groups[SGroupName]{
					plList.append(channel)
					}else {
					groups[SGroupName] = List<Channel>()
					groups[SGroupName]?.append(channel)
					}
					}*/
					
					if(sub != "#"){
						masterPlaylist.channels.append(channel)
					}
				
				}
				
				
				
			}
			
			for (gName,channels) in groups {
				let pl = Playlist()
				pl.name = gName
				pl.isVod = channels[0].isVideo
				pl.channels.append(objectsIn: channels)
				masterPlaylist.subPlaylists.append(pl)
			}
			
			try! realm.write {
				realm.add(playlist)
				dismiss(animated: true, completion: nil)
			}

		}
		
			}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
}
