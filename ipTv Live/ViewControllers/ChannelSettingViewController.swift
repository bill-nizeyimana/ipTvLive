//
//  ChannelSettingViewController.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 03/02/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import RealmSwift

class ChannelSettingViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
	
	var mainPlaylist : MainPlaylist!
	var subPlaylists : Results<Playlist>?
	
	var subPlaylistIndex = 0
	var movieIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
			
			let predicate = NSPredicate(format: "isVod = %@", NSNumber(value: false))
			let sub = mainPlaylist.masterPlaylist?.subPlaylists.filter(predicate).sorted(byKeyPath: "name")
			subPlaylists = sub
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	
	
	 // MARK: - Navigation
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		subPlaylistIndex = indexPath.row
		//collectionView.reloadData()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return (subPlaylists?.count)!
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		let playlist = subPlaylists?[indexPath.row]
		cell.textLabel?.text = playlist?.name
		if let c = playlist?.channels.count {
			cell.detailTextLabel?.text = "\(c)"
		}
		
		return cell
	}
	

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
