//
//  MovieAVPlayerViewController.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 01/02/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MovieAVPlayerViewController: AVPlayerViewController,VLCMediaPlayerDelegate {
	
	var movieUrl : String?
	var isVlc = false
	var mediaPlayer = VLCMediaPlayer()
	
	var timeView : UIView!
	

    override func viewDidLoad() {
        super.viewDidLoad()
			
			let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(MovieAVPlayerViewController.tapped(gesture:)))
			tapRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.playPause.rawValue)];
			self.view.addGestureRecognizer(tapRecognizer)
			
			
			if let _ = movieUrl?.range(of: "mp4"){
				isVlc = false;
				let videoURL = NSURL(string: movieUrl!)
				self.player = AVPlayer(url: videoURL! as URL)
				self.player!.play()
			}else{
				isVlc = true
				mediaPlayer.delegate = self
				mediaPlayer.drawable = self.view
				
				let url = URL(string: movieUrl!)
				
				let media = VLCMedia(url: url!)
				mediaPlayer.media = media
				
				mediaPlayer.play()
			}
    }
	
	
	func tapped(gesture: UITapGestureRecognizer) {
		
		if(isVlc){
			self.showsPlaybackControls = true
			if(mediaPlayer.isPlaying){
				mediaPlayer.pause()
			}else{
				mediaPlayer.play()
			}
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		if(isVlc){
			mediaPlayer.stop()
		}
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 // MARK: - Navigation
	
	
	func mediaPlayerTimeChanged(_ aNotification: Notification!) {
		print(mediaPlayer.time.stringValue)
	}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
