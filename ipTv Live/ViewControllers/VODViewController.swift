//
//  VODViewController.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 01/02/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import RealmSwift

class VODViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource {
	
	var mainPlaylist : MainPlaylist!
	var subPlaylists : Results<Playlist>?
	var subPlaylistIndex = 0
	var movieIndex = 0

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var collectionView: UICollectionView!
	
	
    override func viewDidLoad() {
        super.viewDidLoad()
			
			let predicate = NSPredicate(format: "isVod = %@", NSNumber(value: true))
			let sub = mainPlaylist.masterPlaylist?.subPlaylists.filter(predicate).sorted(byKeyPath: "name")
			subPlaylists = sub
			
			self.collectionView?.register(UINib(nibName: "CoverCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cover")
			
			self.collectionView.register(UINib(nibName: "VodSectionCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header")
			
			//self.tableView.register(UINib(nibName: "VODTableViewCell", bundle: nil), forCellReuseIdentifier: "cellCover")
			
			//self.tableView.register(VODTableViewCell.self, forCellReuseIdentifier: "cellCover")
			
    }
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	
	// MARK: - CollectionView
	
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if (subPlaylists?.count)! > 0 {
			return (subPlaylists![section].channels.count)
		}else {
			return 0
		}
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return  (subPlaylists?.count)!
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cover", for: indexPath) as! CoverCollectionViewCell
		
		let channel =  subPlaylists?[indexPath.section].channels.sorted(byKeyPath: "name")[indexPath.row]
		cell.title = (channel?.name)!
		if let cover = channel?.tvgLogo {
			cell.setCover(url: cover)
		}
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		movieIndex = indexPath.row
		subPlaylistIndex = indexPath.section
		performSegue(withIdentifier: "playMovie", sender: self)
	}
	
	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		var reusableview: UICollectionReusableView?
		
		if (kind == UICollectionElementKindSectionHeader) {
			let cellTitle = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header", for: indexPath) as! VodSectionCollectionReusableView
			
			let playlist = subPlaylists?[indexPath.section]
			cellTitle.title = (playlist?.name)!
			reusableview = cellTitle
			
		}
		return reusableview!
	}

	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if(segue.identifier == "playMovie"){
			
			let dest = segue.destination as! MovieAVPlayerViewController
			let movie = subPlaylists?[subPlaylistIndex].channels.sorted(byKeyPath: "name")[movieIndex];
			dest.movieUrl = movie?.url
			
		}
	}
	
	
	// MARK : - Tableview
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return (subPlaylists?.count)!
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "cellCover", for: indexPath) as! VODTableViewCell
		
		let playlist = subPlaylists?[indexPath.section]
		
		cell.configureCell(listChannel: (playlist?.channels)!)
		
		return cell
		
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		let playlist = subPlaylists?[section]
		return playlist?.name
	}
	

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
