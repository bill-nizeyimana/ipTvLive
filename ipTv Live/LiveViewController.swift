//
//  FirstViewController.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 15/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//
import UIKit
import RealmSwift



class LiveViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource{
	
	@IBOutlet weak var collectionViewProgram: UICollectionView!
	@IBOutlet weak var collectionViewChannel: UICollectionView!
	@IBOutlet weak var collectionViewPlaylist: UICollectionView!
	@IBOutlet weak var viewChannel: UIView!
	@IBOutlet weak var videoView: VideoView!
	var mainPlaylist : MainPlaylist!
	var subPlaylists : Results<Playlist>?
	var subPlaylistIndex = 0
	var channelIndex = 0
	var hidden = false
	var timer = Timer()
	@IBOutlet weak var bottomView: UIView!
	let defaults = UserDefaults.standard
	
	
	@IBOutlet weak var lblTime: UILabel!
	override func viewDidLoad() {
		super.viewDidLoad()
		UIApplication.shared.isIdleTimerDisabled = true
		
		
		self.collectionViewPlaylist.register(UINib(nibName: "PlaylistCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlaylistCollectionViewCell")
		
		
		self.collectionViewChannel.register(UINib(nibName: "ChannelCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ChannelCollectionViewCell")
		
		self.collectionViewProgram.register(UINib(nibName: "ProgramCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProgramCollectionViewCell")
		
		let tapRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(LiveViewController.tapped(gesture:)))
		
		self.view.addGestureRecognizer(tapRecognizer)
		
		self.videoView.addGestureRecognizer(tapRecognizer)
		
		
		let tapRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(LiveViewController.playPause(gesture:)))
		tapRecognizer1.allowedPressTypes = [NSNumber(value: UIPressType.playPause.rawValue)];
		self.view.addGestureRecognizer(tapRecognizer1)
		
		
		timer = Timer.scheduledTimer(timeInterval: TimeInterval(50), target: self, selector: #selector(LiveViewController.hideShow), userInfo: nil, repeats: false)
		
		let predicate = NSPredicate(format: "isVod = %@", NSNumber(value: false))
		let sub = mainPlaylist.masterPlaylist?.subPlaylists.filter(predicate).sorted(byKeyPath: "name")
		subPlaylists = sub
		
		
		
		if let ink = defaults.object(forKey:"channelIndex") {
			channelIndex = ink as! Int
		}
		
		if let ink = defaults.object(forKey:"subPlaylistIndex") {
			subPlaylistIndex = ink as! Int
		}
		
		//DispatchQueue.global(qos: .userInitiated).async {
		 TvGuide.fetchAllGuides(mainPlaylist: self.mainPlaylist)
		//}
		
		self.setTime()
		
		var channel: Channel!
		if(subPlaylistIndex == 0){
			channel = mainPlaylist.masterPlaylist?.channels.sorted(byKeyPath: "name")[channelIndex]
		}else{
			channel =  subPlaylists?[subPlaylistIndex-1].channels[channelIndex]
		}
		
		videoView.play(stringUrl: (channel?.url)!)
		
		collectionViewChannel.reloadData()
		
		collectionViewChannel.scrollToItem(at: IndexPath(row: channelIndex, section: 0), at: .left, animated: true)
		
	}
	
	
	func tapped(gesture: UITapGestureRecognizer) {
		
		if(hidden){
			UIView.animate(withDuration: 0.4, animations: { () -> Void in
				self.viewChannel.alpha = 1.0
				self.bottomView.alpha = 1.0
				//self.collectionView.reloadData()
			}, completion: { (Bool) -> Void in
				self.hidden = false
			})
		}
	}
	
	
	func playPause(gesture: UITapGestureRecognizer) {
		if videoView.isPlaying() {
			videoView.resume()
		}else {
			videoView.pause()
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		UIApplication.shared.isIdleTimerDisabled = false
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		videoView.player.play()
		videoView.mediaPlayer.play()
		self.hidden = true
		hideShow()
		
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		videoView.player.pause()
		if(videoView.mediaPlayer.isPlaying){
			videoView.mediaPlayer.pause()
		}
	}
	
	// MARK: UICollectionViewDelegate
	
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		switch collectionView.tag {
		case 0:
			return (subPlaylists?.count)! + 1
		case 1:
			if(self.subPlaylistIndex == 0){
				return (mainPlaylist.masterPlaylist?.channels.count)!
			}else{
				return (subPlaylists![subPlaylistIndex-1].channels.count)
			}
		case 2:
			
			
			if(self.subPlaylistIndex == 0){
				let channel =  mainPlaylist.masterPlaylist?.channels[channelIndex]
				return (channel?.programs.count)!
			}else{
				let channel =  subPlaylists?[subPlaylistIndex-1].channels[channelIndex]
				return (channel?.programs.count)!
			}
			
		default:
			return 0
		}
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		switch collectionView.tag {
		case 0:
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaylistCollectionViewCell", for: indexPath) as! PlaylistCollectionViewCell
			if(indexPath.row == 0){
				cell.title = "Toutes les chaines"
				if let _ = mainPlaylist.masterPlaylist?.channels.count {
					
				}
			}else{
				let pl = subPlaylists?[indexPath.row-1]
				cell.title  = (pl?.name)!
				if let _ = subPlaylists?[indexPath.row-1].channels.count {
					
				}
			}
			
			return cell
			
		case 1:
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChannelCollectionViewCell", for: indexPath) as! ChannelCollectionViewCell
			var channel :Channel!
			if(subPlaylistIndex == 0){
			 channel =  mainPlaylist.masterPlaylist?.channels.sorted(byKeyPath: "name")[indexPath.row]
			}else{
				channel =  subPlaylists?[subPlaylistIndex-1].channels[indexPath.row]
			}
			cell.channel = channel
			cell.title = channel.name
			cell.setCover(chanel: channel)
			return cell
			
		case 2:
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProgramCollectionViewCell", for: indexPath) as! ProgramCollectionViewCell
			var channel :Channel!
			if(subPlaylistIndex == 0){
			 channel =  mainPlaylist.masterPlaylist?.channels.sorted(byKeyPath: "name")[channelIndex]
			}else{
				channel =  subPlaylists?[subPlaylistIndex-1].channels[channelIndex]
			}
			cell.showCurrent(channel.url)
			cell.setUp(channel: channel,index: indexPath.row)
			return cell
		default:
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaylistCollectionViewCell", for: indexPath)
			return cell
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, canFocusItemAt indexPath: IndexPath) -> Bool {
		if collectionView.tag == 2 {
			return true
		}else{
			return true
		}
	}
	
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		switch collectionView.tag {
		case 0:
			self.subPlaylistIndex = indexPath.row
			collectionViewChannel.reloadData()
			break
		case 1:
			if(videoView.mediaPlayer.isPlaying){
				videoView.mediaPlayer.stop()
			}
			var channel: Channel!
			if(subPlaylistIndex == 0){
				channel = mainPlaylist.masterPlaylist?.channels.sorted(byKeyPath: "name")[indexPath.row]
			}else{
				channel =  subPlaylists?[subPlaylistIndex-1].channels[indexPath.row]
			}
			
			videoView.play(stringUrl: (channel?.url)!)
			break
		default:
			break
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
		
		timer.invalidate()
		timer = Timer.scheduledTimer(timeInterval: TimeInterval(5), target: self, selector: #selector(LiveViewController.hideShow), userInfo: nil, repeats: false)
		
		switch collectionView.tag {
		case 0:
			if let index = context.nextFocusedIndexPath {
				
				subPlaylistIndex = index.row
				defaults.set(subPlaylistIndex, forKey: "subPlaylistIndex")
				collectionViewChannel.reloadData()
			}
			break
		case 1:
			
			if let index = context.nextFocusedIndexPath {
				channelIndex = index.row
				defaults.set(channelIndex, forKey: "channelIndex")
				collectionViewProgram.reloadData()
				
				var count = 0
				if(self.subPlaylistIndex == 0){
					let channel =  mainPlaylist.masterPlaylist?.channels[channelIndex]
					count = (channel?.programs.count)!
				}else{
					let channel =  subPlaylists?[subPlaylistIndex-1].channels[channelIndex]
					count =  (channel?.programs.count)!
				}

				if(count > 0){
					//collectionViewProgram.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: true)
				}
			}
			
			break
		default:
			break
		}
		
	}
	
	
	
	private var viewToFocus: UIView?
	
	 var preferredFocusView: UIView? {
		get {
			return self.viewToFocus
		}
	}
	
	
	func setTime() {
		let formatter = DateFormatter()
		formatter.dateFormat = "HH:mm"
		lblTime.text = formatter.string(from: Date())
	}
	
	
	func hideShow() {
		
			if(hidden){
				UIView.animate(withDuration: 0.4, animations: { () -> Void in
					self.viewChannel.alpha = 1.0
					self.bottomView.alpha = 1.0
					self.setTime()
					self.lblTime.alpha = 1.0
					//self.collectionView.reloadData()
				}, completion: { (Bool) -> Void in
					self.hidden = false
				})
			}else{
				UIView.animate(withDuration: 0.5, animations: { () -> Void in
					self.viewChannel.alpha = 0.0
					self.bottomView.alpha = 0.0
					self.lblTime.alpha = 0.0
					
				}, completion: { (Bool) -> Void in
					self.hidden = true
				})
			}
		
	}
	
	//
	
}
