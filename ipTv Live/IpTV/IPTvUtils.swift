//
//  IPTvUtils.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 10/02/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation
import RealmSwift

class IPTvUtils {
	
	static func updateVOD(mainPlaylist : MainPlaylist!){
		
		let model = try? M3U8PlaylistModel(url: mainPlaylist.url)
		var groups = [String:List<Channel>]()
		let channels = List<Channel>()
		
		if(model != nil){
			
			deleteVOD(mainPlaylist: mainPlaylist)
			
			let segList = model?.mainMediaPl.segmentList
			var count = 1
			count = Int((segList?.count)!)
			var SGroupName = ""
			
			for index in 0...count-1 {
				let info = segList?.segmentInfo(at: UInt(index))
				let title = (info?.title)!
				let uri = (info?.uri)!
				let sub = "\(title[title.startIndex])"
				
				if(sub != "#" && isVideo(url: uri)){
					let channel = Channel()
					channel.name = title
					channel.url = (info?.uri)!
					channel.duration = (info?.duration)!
					channel.tagInfo = (info?.tagInfo)!
					channel.name = ChannelUtils.fixName(name: channel.name)
					
					let str: NSMutableString = NSMutableString(string: channel.name)
					CFStringTransform(str, nil, kCFStringTransformStripDiacritics, false)
					channel.flatName = str as String
					
					if let rGroup = channel.tagInfo.range(of: "tvg-logo=\""){
						let rm = channel.tagInfo.substring(from: rGroup.upperBound)
						if let rmGroup = rm.range(of: "\""){
							let tvgLogo = rm.substring(to: rmGroup.lowerBound)
							channel.tvgLogo = tvgLogo
						}
					}
					
					if let tivo = ChannelUtils.getTivoChannel(channel: channel) {
						channel.tivoChannel = tivo
						channel.tvgLogo = tivo.ImageUrl
						channel.sourceId = Int(tivo.id!)!
						channel.channelId = tivo.id
					}
					
					if (channel.url.lowercased().range(of:"ts") == nil) && (channel.url.lowercased().range(of:"m3u8") == nil) {
						channel.isVideo = true
					}
					if let rGroup = channel.tagInfo.range(of: "group-title=\""){
						
						let rm = channel.tagInfo.substring(from: rGroup.upperBound)
						if let rmGroup = rm.range(of: "\""){
							
							var groupName = rm.substring(to: rmGroup.lowerBound)
							groupName = Utils.removeSpecialCharsFromString(text: groupName)
							groupName = groupName.trimmingCharacters(in: .whitespaces)
							
							
							if(groupName != ""){
								let f = "\(groupName[groupName.startIndex])"
								if(f != "#"){
									
									if let plList = groups[groupName]{
										plList.append(channel)
									}else {
										groups[groupName] = List<Channel>()
										groups[groupName]?.append(channel)
									}
								}
							}
							
						}
					}
					
					
					var sub = "\(channel.name[channel.name.startIndex])"
					
					sub = sub.trimmingCharacters(in: .whitespaces)
					let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
					if sub.rangeOfCharacter(from: characterset.inverted) != nil {
						SGroupName = Utils.removeSpecialCharsFromString(text: channel.name)
						SGroupName = SGroupName.trimmingCharacters(in: .whitespaces)
						
					}
					
					channels.append(channel)
				}
				
			}
			
			
			let subPlaylists = List<Playlist>()
			
			for (gName,channels) in groups {
				let pl = Playlist()
				pl.name = gName
				pl.isVod = channels[0].isVideo
				pl.channels.append(objectsIn: channels)
				
				subPlaylists.append(pl)
				
			}
			
			
			let realm = try! Realm()
			try! realm.write {
				mainPlaylist.masterPlaylist?.channels.append(objectsIn: channels)
				mainPlaylist.masterPlaylist?.subPlaylists.append(objectsIn: subPlaylists)
				}
			}		
	}
	
	static func updateTV(mainPlaylist : MainPlaylist!){
		
		let model = try? M3U8PlaylistModel(url: mainPlaylist.url)
		var groups = [String:List<Channel>]()
		
		if(model != nil){
			
			deleteTV(mainPlaylist: mainPlaylist)
			let realm = try! Realm()
			let segList = model?.mainMediaPl.segmentList
			var count = 1
			count = Int((segList?.count)!)
			var SGroupName = ""
			
			for index in 0...count-1 {
				let info = segList?.segmentInfo(at: UInt(index))
				let title = (info?.title)!
				let uri = (info?.uri)!
				let sub = "\(title[title.startIndex])"
				
				if(sub != "#" && !isVideo(url: uri)){
					let channel = Channel()
					channel.name = title
					channel.url = (info?.uri)!
					channel.duration = (info?.duration)!
					channel.tagInfo = (info?.tagInfo)!
					channel.name = ChannelUtils.fixName(name: channel.name)
					
					let str: NSMutableString = NSMutableString(string: channel.name)
					CFStringTransform(str, nil, kCFStringTransformStripDiacritics, false)
					channel.flatName = str as String
					
					if let rGroup = channel.tagInfo.range(of: "tvg-logo=\""){
						let rm = channel.tagInfo.substring(from: rGroup.upperBound)
						if let rmGroup = rm.range(of: "\""){
							let tvgLogo = rm.substring(to: rmGroup.lowerBound)
							channel.tvgLogo = tvgLogo
						}
					}
					
					if let tivo = ChannelUtils.getTivoChannel(channel: channel) {
						channel.tivoChannel = tivo
						channel.tvgLogo = tivo.ImageUrl
						channel.sourceId = Int(tivo.id!)!
						channel.channelId = tivo.id
					}
					
					if (channel.url.lowercased().range(of:"ts") == nil) && (channel.url.lowercased().range(of:"m3u8") == nil) {
						channel.isVideo = true
					}
					if let rGroup = channel.tagInfo.range(of: "group-title=\""){
						
						let rm = channel.tagInfo.substring(from: rGroup.upperBound)
						if let rmGroup = rm.range(of: "\""){
							
							var groupName = rm.substring(to: rmGroup.lowerBound)
							groupName = Utils.removeSpecialCharsFromString(text: groupName)
							groupName = groupName.trimmingCharacters(in: .whitespaces)
							
							
							if(groupName != ""){
								let f = "\(groupName[groupName.startIndex])"
								if(f != "#"){
									
									if let plList = groups[groupName]{
										plList.append(channel)
									}else {
										groups[groupName] = List<Channel>()
										groups[groupName]?.append(channel)
									}
								}
							}
							
						}
					}
					
					
					var sub = "\(channel.name[channel.name.startIndex])"
					
					sub = sub.trimmingCharacters(in: .whitespaces)
					let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
					if sub.rangeOfCharacter(from: characterset.inverted) != nil {
						SGroupName = Utils.removeSpecialCharsFromString(text: channel.name)
						SGroupName = SGroupName.trimmingCharacters(in: .whitespaces)
						
					}
					
					if(sub != "#"){
						try! realm.write {
							mainPlaylist.masterPlaylist?.channels.append(channel)
						}
					}
				}
				
			}
			
			for (gName,channels) in groups {
				let pl = Playlist()
				pl.name = gName
				pl.isVod = channels[0].isVideo
				pl.channels.append(objectsIn: channels)
				try! realm.write {
					mainPlaylist.masterPlaylist?.subPlaylists.append(pl)
				}
			}
			
		}
		
	}
	
	static func isVideo(url:String) -> Bool{
		if (url.lowercased().range(of:"ts") == nil) && (url.lowercased().range(of:"m3u8") == nil) {
			return true
		}else{
			return false
		}
	}
	
	static func deleteVOD(mainPlaylist : MainPlaylist!){
		let realm = try! Realm()
		let predicate = NSPredicate(format: "isVod = %@", NSNumber(value: true))
		
		let sub = mainPlaylist.masterPlaylist?.subPlaylists.filter(predicate)
		try! realm.write {
			realm.delete(sub!)
		}
		
		let predicate2 = NSPredicate(format: "isVideo = %@", NSNumber(value: true))
		
		let mov = mainPlaylist.masterPlaylist?.channels.filter(predicate2)
		try! realm.write {
			realm.delete(mov!)
		}
	}
	
	
	static func deleteTV(mainPlaylist : MainPlaylist!){
		let realm = try! Realm()
		let predicate = NSPredicate(format: "isVod = %@", NSNumber(value: false))
		
		let sub = mainPlaylist.masterPlaylist?.subPlaylists.filter(predicate)
		try! realm.write {
			realm.delete(sub!)
		}
		
		let predicate2 = NSPredicate(format: "isVideo = %@", NSNumber(value: false))
		
		let mov = mainPlaylist.masterPlaylist?.channels.filter(predicate2)
		try! realm.write {
			realm.delete(mov!)
		}
	}
}
