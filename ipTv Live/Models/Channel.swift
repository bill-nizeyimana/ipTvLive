//
//  Channel.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 18/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation

import RealmSwift

// Dog model
class Channel: Object {
	
	dynamic var name = ""
	dynamic var flatName = ""
	dynamic var url = ""
	dynamic var tagInfo = ""
	dynamic var isVideo = false
	var duration = -1 as  TimeInterval
	dynamic var channelId : String?
	dynamic var tvgLogo : String?
	var sourceId : Int?
	dynamic var createdAt = Date()
	var tivoChannel :TivoChannel?
	var programs = List<Program>()
	let playlist = LinkingObjects(fromType: Playlist.self, property: "channels")
	
	override static func indexedProperties() -> [String] {
		return ["name"]
	}
	
	override static func primaryKey() -> String? {
		return "url"
	}
}
