//
//  Program.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 31/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation

import RealmSwift

// TivoChannel model
class Program: Object {
	
	dynamic var desc = ""
	dynamic var episodeTitle = ""
	dynamic var title = ""
	dynamic var programId = ""
	dynamic var duration = 0
	dynamic var sourceId = 0
	dynamic var offset = 0
	dynamic var serviceId = 0
	dynamic var startDate : Date?
	dynamic var endDate : Date?
	
	override static func indexedProperties() -> [String] {
		return ["programId","startDate"]
	}
	
}
