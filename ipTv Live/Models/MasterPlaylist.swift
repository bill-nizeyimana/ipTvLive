//
//  MasterPlaylist.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 18/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation
import RealmSwift

class MasterPlaylist: Object {
	dynamic var name = ""
	dynamic var url = ""
	dynamic var originalText = ""
	dynamic var createdAt = Date()
	var channels = List<Channel>()
	var subPlaylists = List<Playlist>()
	var favorites :Favorite?
}
