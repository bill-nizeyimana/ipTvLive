//
//  TivoChannel.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 30/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation

import RealmSwift

// TivoChannel model
class TivoChannel: Object {
	
	dynamic var DisplayName = ""
	dynamic var FullName = ""
	dynamic var flatName = ""
	dynamic var id : String?
	var SourceId : Int?
	dynamic var ImageUrl : String?
	
	override static func indexedProperties() -> [String] {
		return ["DisplayName"]
	}
	
	override static func primaryKey() -> String? {
		return "id"
	}
	
}

