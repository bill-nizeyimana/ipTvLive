//
//  Playlist.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 18/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation

import RealmSwift

// Dog model
class MainPlaylist: Object {
	dynamic var name = ""
	dynamic var url = ""
	dynamic var createdAt = Date()
	dynamic var masterPlaylist : MasterPlaylist?
}
