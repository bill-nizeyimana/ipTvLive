//
//  Favorite.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 18/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation

import RealmSwift

class Favorite: Object {
	dynamic var name = ""
	dynamic var createdAt = Date()
	var channels = List<Channel>()
	var subPlaylist = List<Playlist>()

}
