//
//  TagInfo.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 19/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation

import RealmSwift

class TagInfo: Object {
	dynamic var logo = ""
	dynamic var groupTitle = ""
}
