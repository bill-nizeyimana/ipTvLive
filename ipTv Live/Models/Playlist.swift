//
//  Playlist.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 18/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation

import RealmSwift

// Dog model
class Playlist: Object {
	dynamic var name = ""
	dynamic var url = ""
	dynamic var isVod = false
	dynamic var createdAt = Date()
	let channels = List<Channel>()
}
