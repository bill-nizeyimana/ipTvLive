//
//  File.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 19/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
	func drawInRectAspectFill(rect: CGRect) {
		let targetSize = rect.size
		
		let widthRatio    = targetSize.width  / self.size.width
		let heightRatio   = targetSize.height / self.size.height
		let scalingFactor = max(widthRatio, heightRatio)
		let newSize = CGSize(width:  self.size.width  * scalingFactor,
		                     height: self.size.height * scalingFactor)
		UIGraphicsBeginImageContext(targetSize)
		let origin = CGPoint(x: (targetSize.width  - newSize.width)  / 2,
		                     y: (targetSize.height - newSize.height) / 2)
		self.draw(in: CGRect(origin: origin, size: newSize))
		let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		scaledImage?.draw(in: rect)
	}
}

class Utils {
	
	static func imageWithView(view : UIView!) -> UIImage {
	UIGraphicsBeginImageContextWithOptions(view.bounds.size, true, 0.0)
		view.layer.render(in: UIGraphicsGetCurrentContext()!)
		let img = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return img!
	}
	
	
	static func removeSpecialCharsFromString(text: String) -> String {
		let okayChars : Set<Character> =
			Set("abcdefghijklmnopqrstuvwxyzéèàçîï ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890".characters)
		return String(text.characters.filter {okayChars.contains($0) })
	}

}

extension Date {
	/// Returns the amount of years from another date
	func years(from date: Date) -> Int {
		return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
	}
	/// Returns the amount of months from another date
	func months(from date: Date) -> Int {
		return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
	}
	/// Returns the amount of weeks from another date
	func weeks(from date: Date) -> Int {
		return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
	}
	/// Returns the amount of days from another date
	func days(from date: Date) -> Int {
		return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
	}
	/// Returns the amount of hours from another date
	func hours(from date: Date) -> Int {
		return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
	}
	/// Returns the amount of minutes from another date
	func minutes(from date: Date) -> Int {
		return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
	}
	/// Returns the amount of seconds from another date
	func seconds(from date: Date) -> Int {
		return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
	}
	/// Returns the a custom time interval description from another date
	func offset(from date: Date) -> String {
		if years(from: date)   > 0 { return "\(years(from: date))y"   }
		if months(from: date)  > 0 { return "\(months(from: date))M"  }
		if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
		if days(from: date)    > 0 { return "\(days(from: date))d"    }
		if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
		if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
		if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
		return ""
	}
}
