//
//  ChannelUtils.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 10/02/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import Foundation
import RealmSwift

class ChannelUtils {
	static func fixName(name: String) -> String {
		var nameFix = name
		nameFix = nameFix.replacingOccurrences(of: "_", with: " ")
		if let rGroup = nameFix.range(of: ":"){
			nameFix = nameFix.substring(from: rGroup.upperBound)
		}
		
		if let rGroup = nameFix.range(of: "\r"){
			nameFix = nameFix.substring(to: rGroup.lowerBound)
		}
		
		nameFix = nameFix.replacingOccurrences(of: " fr", with: "")
		nameFix = nameFix.replacingOccurrences(of: " FR", with: "")
		
		nameFix = nameFix.trimmingCharacters(in: .whitespaces)
		
		return nameFix
	}
	
	static func getTivoChannel(channel : Channel) -> TivoChannel? {
		var tivo : TivoChannel? = nil
		
		let realm1 = try! Realm()
		
		var flat = channel.flatName;
		flat = flat.replacingOccurrences(of: "Full HD", with: "")
		flat = flat.replacingOccurrences(of: " DECALE-9", with: "")
		flat = flat.replacingOccurrences(of: " DECALE-6", with: "")
		flat = flat.replacingOccurrences(of: " -6", with: "")
		flat = flat.replacingOccurrences(of: " fr", with: "")
		let predicate = NSPredicate(format: "DisplayName CONTAINS[c] %@ OR flatName CONTAINS[c] %@ ", channel.flatName, flat)
		
		let tanDogs = realm1.objects(TivoChannel.self).filter(predicate)
		
		if tanDogs.count > 0 {
			tivo = tanDogs[0]
		}
		return tivo
	}
	
}
