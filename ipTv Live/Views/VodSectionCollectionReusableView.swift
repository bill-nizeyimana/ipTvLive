//
//  VodSectionCollectionReusableView.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 10/02/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit

class VodSectionCollectionReusableView: UICollectionReusableView {
        
	@IBOutlet weak var lblTitle: UILabel!
	
	var title : String {
		get{
			return lblTitle.text!
		}
		set(newTitle){
			lblTitle.text = newTitle
		}
	}
}
