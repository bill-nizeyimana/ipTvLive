//
//  MainPlaylistCollectionViewCell.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 18/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit



class MainPlaylistCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var lblTitle: UILabel!
	@IBOutlet weak var imgPlaylist: UIImageView!
	
	var title : String {
		get{
			return lblTitle.text!
		}
		set(newtitle){
			lblTitle.text = newtitle
			
			imgPlaylist.agc_setImageWithInitials(fromName: newtitle)
			
			imgPlaylist.image = Utils.imageWithView(view: imgPlaylist)
			
		}
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		// These properties are also exposed in Interface Builder.
		imgPlaylist.adjustsImageWhenAncestorFocused = true
		imgPlaylist.clipsToBounds = false
		
		lblTitle.alpha = 0.0
		
	}
	
	
	override func prepareForReuse() {
		super.prepareForReuse()
		
		// Reset the label's alpha value so it's initially hidden.
		lblTitle.alpha = 0.0
		
	}
	
	override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
		coordinator.addCoordinatedAnimations({
			if self.isFocused {
				
				self.lblTitle.alpha = 1.0
			}
			else {
				
				self.lblTitle.alpha = 0.0
			}
		}, completion: nil)
	}
	
}
