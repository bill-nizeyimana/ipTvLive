//
//  ProgramCollectionViewCell.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 23/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit

class ProgramCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var progressTime: UIProgressView!
	@IBOutlet weak var lblEnd: UILabel!
	@IBOutlet weak var lblStart: UILabel!
	@IBOutlet weak var lblDescription: UILabel!
	@IBOutlet weak var lblSubtitle: UILabel!
	@IBOutlet weak var lblTitle: UILabel!
	@IBOutlet weak var videoView: VideoView!
	var channel : Channel!
	
	
	override func awakeFromNib() {
		super.awakeFromNib()
		//videoView.mute()
	}
	
	func configure(channel: Channel)  {
		self.channel = channel
	}
	
	override func prepareForReuse() {
		super.prepareForReuse()
		lblTitle.alpha = 0.0
		//videoView.pause()
	}
	
	func setUp(channel : Channel,index : Int)  {
		if channel.programs.count > 0 {
			let pred = NSPredicate(format: "endDate > %@", Date() as CVarArg)
		let progs = channel.programs.filter(pred).sorted(byKeyPath: "startDate")
			if progs.count > 0 && progs.count > index {
				let prog = progs[index]
				lblTitle.text = prog.title
				lblTitle.alpha = 1.0
				
				lblSubtitle.text = prog.episodeTitle
				lblDescription.text = prog.desc
				
				let formatter = DateFormatter()
				formatter.dateFormat = "HH:mm"
				
				if let sDate = prog.startDate {
					self.lblStart.text = formatter.string(from: sDate)
					self.lblEnd.text = formatter.string(from: prog.endDate!)
					
					let time = Date().minutes(from: sDate)
					let cu = Float((Float(time)/Float(50))) as Float
					progressTime.progress = Float(cu)
				}
				
			
			}
		
	}
	}
	
	func showCurrent(_ url:String)  {
		//videoView.play(stringUrl: url)
	}
	
	override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
		coordinator.addCoordinatedAnimations({
			if self.isFocused {
				
				self.layer.borderColor = UIColor.white.cgColor
				self.layer.borderWidth = 2
			}
			else {
				self.layer.borderColor = UIColor.white.cgColor
				self.layer.borderWidth = 0
			}
		}, completion: nil)
	}
}
