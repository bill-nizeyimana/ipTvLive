//
//  VODTableViewCell.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 11/02/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import RealmSwift

class VODTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

	@IBOutlet weak var collectionView: UICollectionView!
	var channels = List<Channel>()
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
				self.collectionView?.register(UINib(nibName: "CoverCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cover")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	
	func configureCell(listChannel:List<Channel>) {
		self.channels = listChannel
		collectionView.reloadData()
	}
	
	// MARK: - CollectionView
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return channels.count
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return  1
	}
	
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cover", for: indexPath) as! CoverCollectionViewCell
		let channel =  channels[indexPath.row]
		cell.title = (channel.name)
		if let cover = channel.tvgLogo {
			cell.setCover(url: cover)
		}
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
	
	return CGSize(width: 100, height: 200)
	}

}
