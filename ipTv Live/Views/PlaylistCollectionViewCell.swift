//
//  PlaylistCollectionViewCell.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 23/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit

class PlaylistCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var lblLogo: UILabel!
	@IBOutlet weak var lblTitle: UILabel!
	
	var title : String {
		get{
			return lblTitle.text!
		}
		
		set(newTitle){
			lblTitle.text = newTitle;
		}
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		self.layer.cornerRadius = 4
		self.lblTitle.textColor = UIColor.gray
		//lblTitle.alpha = 0.0
		
	}
	
	
	override func prepareForReuse() {
		super.prepareForReuse()
		
		// Reset the label's alpha value so it's initially hidden.
		
		lblTitle.alpha = 0.0
		
	}
	
	override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
		coordinator.addCoordinatedAnimations({
			if self.isFocused {
				self.lblTitle.alpha = 1.0
				self.lblTitle.textColor = UIColor.white
				self.layer.borderColor = UIColor.white.cgColor
				self.layer.borderWidth = 2
			}
			else {
				self.lblTitle.textColor = UIColor.gray
				self.layer.borderColor = UIColor.white.cgColor
				self.layer.borderWidth = 0
				//self.lblTitle.alpha = 0.0
			}
		}, completion: nil)
	}
}
