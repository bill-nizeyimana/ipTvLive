//
//  ChannelCollectionViewCell.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 23/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import SDWebImage

class ChannelCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var lblTitle: UILabel!
	@IBOutlet weak var imgChannel: UIImageView!
	var channel : Channel!
	
	var title : String {
		get{
			return lblTitle.text!
		}
		set(newtitle){
			lblTitle.text = newtitle
			
			//imgChannel.agc_setImageWithInitials(fromName: newtitle)
			
		//	imgChannel.image = Utils.imageWithView(view: imgChannel)
			
		}
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		self.imgChannel.contentMode = .scaleAspectFit
		//self.imgChannel.image?.drawInRectAspectFill(rect: CGRect(x: 0, y: 0, width: 50, height: 50))
	}
	
	func setCover(url:String) {
		imgChannel.sd_showActivityIndicatorView()
		let u = "https://api.backendless.com/3927A13B-6239-E324-FFCB-87788A5E9C00/v1/files/TVLOGOS/disneychal.png"
		imgChannel.sd_setImage(with: URL(string: u), placeholderImage: UIImage(named: "movie-placeholder.jpg"))
	}
	
	func setCover(chanel:Channel) {
		imgChannel.sd_showActivityIndicatorView()
		var id = ""
		var url = ""
		
		if(chanel.channelId != nil){
			id = chanel.channelId!
			 url = "https://api.backendless.com/3927A13B-6239-E324-FFCB-87788A5E9C00/v1/files/TVLOGOS/\(id).png"
		}else {
			if(chanel.isVideo){
				if let u = chanel.tvgLogo {
					url = u
				}else {
					url = "http://gamescast.tv/press/media/tv.png"
				}
			}else{
				
			id = chanel.flatName.replacingOccurrences(of: " ", with: "_")
				url = "https://api.backendless.com/3927A13B-6239-E324-FFCB-87788A5E9C00/v1/files/TVLOGOS/\(id).png"
			}
		}
		
		
		imgChannel.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "movie-placeholder.jpg"))
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		// These properties are also exposed in Interface Builder.
		//imgChannel.adjustsImageWhenAncestorFocused = true
		imgChannel.clipsToBounds = false
		
		lblTitle.alpha = 0.0
		self.lblTitle.textColor = UIColor.white
		
	}
	
	
	override func prepareForReuse() {
		super.prepareForReuse()
		
		// Reset the label's alpha value so it's initially hidden.
		self.lblTitle.textColor = UIColor.white
		imgChannel.image = UIImage(named: "movie-placeholder.jpg")
		lblTitle.alpha = 0.0
		self.layer.borderWidth = 0
		
		
	}
	
	override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
		coordinator.addCoordinatedAnimations({
			if self.isFocused {
				
				self.lblTitle.alpha = 1.0
				self.lblTitle.textColor = UIColor.white
				self.layer.borderColor = UIColor.white.cgColor
				self.layer.borderWidth = 3
				
				
			}
			else {
				self.lblTitle.textColor = UIColor.white
				self.lblTitle.alpha = 0.0
				self.layer.borderWidth = 0
				
			}
		}, completion: nil)
	}

	
}
