//
//  CoverCollectionViewCell.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 01/02/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import SDWebImage

class CoverCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var imgCover: UIImageView!
	@IBOutlet weak var lblTitle: UILabel!
	
	var title : String {
		get{
			return lblTitle.text!
		}
		set(newtitle){
			lblTitle.text = newtitle
		}
	}
	
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		// These properties are also exposed in Interface Builder.
		imgCover.adjustsImageWhenAncestorFocused = true
		imgCover.clipsToBounds = false
		lblTitle.alpha = 0.0
	}
	
	func setCover(url:String) {
		imgCover.sd_showActivityIndicatorView()
		imgCover.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "movie-placeholder.jpg"))
	}

	override func prepareForReuse() {
		super.prepareForReuse()
		imgCover.image = UIImage(named: "movie-placeholder.jpg")
		// Reset the label's alpha value so it's initially hidden.
		lblTitle.alpha = 0.0
		
	}
	
	override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
		coordinator.addCoordinatedAnimations({
			if self.isFocused {
				
				self.lblTitle.alpha = 1.0
			}
			else {
				
				self.lblTitle.alpha = 0.0
			}
		}, completion: nil)
	}
	
}
