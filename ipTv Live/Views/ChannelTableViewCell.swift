//
//  ChannelTableViewCell.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 19/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit

class ChannelTableViewCell: UITableViewCell {

	@IBOutlet weak var lblTitle: UILabel!
	@IBOutlet weak var videoView: VideoView!
	var channel : Channel?
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
			videoView.mediaPlayer.audio.isMuted = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

		
    }
	

}
