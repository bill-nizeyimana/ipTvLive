//
//  AppDelegate.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 15/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		//AVPLicense.register("cqdkmfXwl5r4fJ82ehiDuY1sDZ2wZHa5+TymB0iatZicTdn8QtT2AXCKCAHxxTkMxQas/uDRhzpakMZ7bJIjh0PRC24b9Xw96ninPv+1NXG4gqX+3iRKmt0+Kuv7SJKsTMgD6E9YfkZ/M+u/G5PveciPl7DpFIG4o+DZwZ4o+xEB7dsfoOzUKx8fOBTir9Obh3KMpmXxRiCPXUtTOgTZjtlf3NBsKzu14iwtMvNubXU=")
		
		
		let config = Realm.Configuration(
			// Set the new schema version. This must be greater than the previously used
			// version (if you've never set a schema version before, the version is 0).
			schemaVersion: 13,
			
			// Set the block which will be called automatically when opening a Realm with
			// a schema version lower than the one set above
			migrationBlock: { migration, oldSchemaVersion in
    // We haven’t migrated anything yet, so oldSchemaVersion == 0
    if (oldSchemaVersion < 13) {
			// Nothing to do!
			// Realm will automatically detect new properties and removed properties
			// And will update the schema on disk automatically
    }
  })
		
		// Tell Realm to use this new configuration object for the default Realm
		Realm.Configuration.defaultConfiguration = config
		
		// Now that we've told Realm how to handle the schema change, opening the file
		// will automatically perform the migration
		
	
		//TivoApi.linearSchedule(parameter: parm)
		
		
		let defaults = UserDefaults.standard
		
		let channelsSaved = defaults.object(forKey:"channelsSaved") as? Bool ?? false
		
		if(!channelsSaved){
			
			let path = Bundle.main.path(forResource: "tvList", ofType: "json")
			let url = URL(fileURLWithPath: path!)
			let data = try? Data(contentsOf: url)
			let realm = try! Realm()
			
			if((data) != nil){
				let json = try? JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
				if let ChannelLineup = json?["ServiceDetailsResult"]?["ChannelLineup"] as? [String: AnyObject] {
					if let channels = ChannelLineup["Channels"] as? [[String: AnyObject]]{
						
						for channel in channels {
							let chan = TivoChannel()
							chan.DisplayName = channel["DisplayName"] as! String
							chan.FullName = channel["FullName"] as! String
							chan.SourceId = (channel["SourceId"] as! Int?)!
							chan.id = "\((channel["SourceId"] as! Int?)!)"
							
							chan.DisplayName = chan.DisplayName.replacingOccurrences(of: "-", with: " ")
							
							let str: NSMutableString = NSMutableString(string: chan.FullName)
							CFStringTransform(str, nil, kCFStringTransformStripDiacritics, false)
							
							chan.flatName = str as String
							
							if let imgUrl = channel["ChannelImages"] as? [[String: AnyObject]]{
								chan.ImageUrl = imgUrl[0]["ImageUrl"] as? String
							}
							
							try! realm.write {
								realm.add(chan)
							}
						}
						defaults.set(true, forKey: "channelsSaved")
					}
				}
			}
		}
		return true
	}
	
	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	}
	
	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}
	
	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}
	
	
}

