//
//  VideoView.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 15/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation



class VideoView: UIView, VLCMediaPlayerDelegate {

	var player : AVPlayer{
		get{
			return (self.layer as! AVPlayerLayer).player!
		}
		set(newPlayer){
			(self.layer as! AVPlayerLayer).player = newPlayer
		}
	}
	
	var mediaPlayer = VLCMediaPlayer()
	
	var item : AVPlayerItem!
 
	override class var layerClass: Swift.AnyClass   {
		get{
			return AVPlayerLayer.self
		}
	}
	
	var url : String?
	required override init(frame: CGRect) {
		super.init(frame: frame)
		self.player = AVPlayer()
		mediaPlayer.delegate = self
		mediaPlayer.drawable = self
		
		
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.player = AVPlayer()
		mediaPlayer.delegate = self
		mediaPlayer.drawable = self
	}
	
	func mute() {
		mediaPlayer.audio.isMuted = true
		player.isMuted = true
		
	}
	
	func pause()  {
		mediaPlayer.pause()
		player.pause()
		
	}
	
	func resume() {
		mediaPlayer.play()
		player.play()
	
		
	}
	
	func isPlaying() -> Bool {
		var isPlaying = false
		isPlaying = mediaPlayer.isPlaying || self.player.timeControlStatus == .playing
		return isPlaying
	}
	
	func play(stringUrl: String ){
		self.url = stringUrl
		let url = URL(string: stringUrl)
		
		self.player.pause()
		if(mediaPlayer.isPlaying){
			mediaPlayer.stop()
		}
		
		if let _ = stringUrl.range(of: "m3u8"){
			self.item = AVPlayerItem(url: url!)
			self.player.replaceCurrentItem(with: item)
			self.player.play()

		}else{
						let media = VLCMedia(url: url!)
			mediaPlayer.media = media
		  mediaPlayer.play()
			
		}
	}
	
	func mediaPlayerStateChanged(_ aNotification: Notification!) {
		if mediaPlayer.state == .ended {
			if let u = self.url {
     self.play(stringUrl: u)
			}
		}
	}
}

