//
//  VideoView.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 15/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

class VideoView: UIView {

	var player : AVPlayer{
		get{
			return (self.layer as! AVPlayerLayer).player!
		}
		set(newPlayer){
			(self.layer as! AVPlayerLayer).player = newPlayer
		}
	}
	
	var item : AVPlayerItem!
 
	override class var layerClass: Swift.AnyClass   {
		get{
			return AVPlayerLayer.self
		}
	}
	
	required override init(frame: CGRect) {
		super.init(frame: frame)
		self.player = AVPlayer()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.player = AVPlayer()
	}
	
	func play(stringUrl: String ){
		let url_ = NSURL(string: stringUrl)
		self.item = AVPlayerItem(url: url_ as! URL)
		self.player.replaceCurrentItem(with: item)
		self.player.play()
	}

}
