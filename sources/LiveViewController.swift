//
//  FirstViewController.swift
//  ipTv Live
//
//  Created by Bill Nizeyimana on 15/01/2017.
//  Copyright © 2017 Bill Nizeyimana. All rights reserved.
//

import UIKit
 

class LiveViewController: UIViewController {

	@IBOutlet var videoView: VideoView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let swipeRecognizer = UISwipeGestureRecognizer(target: self, action:   #selector(LiveViewController.swiped))
		
		self.view.addGestureRecognizer(swipeRecognizer)
	//	videoView.play(stringUrl: "http://hls-live-m6-l3.canal-plus.com/live/hls/itele-clair-hd-andr7/and-hd-clair/index.m3u8")
		
		/*let builder = ManifestBuilder()
		if let url = NSURL(string: "http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8") {
			let manifest = builder.parse(url)
		}*/

	}
	
	func swiped(gesture: UIGestureRecognizer) {
		
		if let swipeGesture = gesture as? UISwipeGestureRecognizer {
			
			switch swipeGesture.direction {
				
			case UISwipeGestureRecognizerDirection.right:
				print("swiped right")
			case UISwipeGestureRecognizerDirection.up:
				print("swiped up")
			default:
				break
				
			}
			
		}
		
	}
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}


}

